#include <Windows.h>
#include <iostream>
#include <vector>
#include <TlHelp32.h>
#include <tchar.h>
#include <chrono>
#include <thread>
#include <cmath>
#include <ShlObj.h>
#include <sstream>
#include <fstream>

using namespace std;

DWORD FindProcessIDByName(LPCTSTR ProcessName)
{
  PROCESSENTRY32 pt;
  HANDLE hsnap = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
  pt.dwSize = sizeof(PROCESSENTRY32);
  if (Process32First(hsnap, &pt)) { // must call this first
    do {
      if (!lstrcmpi(pt.szExeFile, ProcessName)) {
        CloseHandle(hsnap);
        return pt.th32ProcessID;
      }
    } while (Process32Next(hsnap, &pt));
  }
  CloseHandle(hsnap); // close handle on failure
  return 0;
}

DWORD_PTR GetAddressFromSignature(HANDLE pHandle, DWORD_PTR pBaseAddress, DWORD_PTR pSize, vector<int> signature) {
  if (pBaseAddress == NULL || pHandle == NULL) {
    return NULL;
  }
  std::vector<byte> memBuffer(pSize);
  if (!ReadProcessMemory(pHandle, (LPCVOID)(pBaseAddress), memBuffer.data(), pSize, NULL)) {
    std::cout << GetLastError() << std::endl;
    return NULL;
  }
  for (DWORD_PTR i = 0; i < pSize; i++) {
    for (DWORD_PTR j = 0; j < signature.size(); j++) {
      if (signature.at(j) != -1 && signature.at(j) != memBuffer.at(i + j))
        //std::cout << std::hex << signature.at(j) << std::hex << memBuffer[i + j] << std::endl;
        break;
      /*if (signature.at(j) == memBuffer.at(i + j) && j > 0)
        std::cout << std::hex << int(signature.at(j)) << std::hex << int(memBuffer.at(i + j)) << j << std::endl;*/
      if (j + 1 == signature.size())
        return pBaseAddress + i;
    }
  }
  return NULL;
}

void ReadPointerChain(
  _In_ HANDLE pHandle,
  _In_ DWORD_PTR baseAddress,
  _In_ int moduleOffset,
  _In_ int* offsets,
  _In_ int offsetCount,
  _Out_writes_bytes_all_(resultByteCount) byte* result,
  _In_ int resultByteCount
) {
  DWORD_PTR pointerChainBaseAddress;
  ReadProcessMemory(pHandle, (LPCVOID)(baseAddress + moduleOffset), &pointerChainBaseAddress, sizeof(pointerChainBaseAddress), NULL);

  DWORD_PTR base = pointerChainBaseAddress;
  DWORD_PTR address;
  for (int i = 0; i < offsetCount - 1; i++) {
    int offset = offsets[i];
    address = base + offset;
    ReadProcessMemory(pHandle, (LPCVOID)(address), &base, sizeof(base), NULL);
  }
  address = base + offsets[offsetCount - 1];
  ReadProcessMemory(pHandle, (LPCVOID)(address), result, resultByteCount, NULL);
};

float ReadFloat(
  _In_ HANDLE pHandle,
  _In_ DWORD_PTR baseAddress,
  _In_ int moduleOffset,
  _In_ int coordinateOffset
) {
  byte* result = (byte*)malloc(4);
  int offsets[] = {
    0x8,
    0x48,
    coordinateOffset
  };
  float res;
  bool validResult = false;
  while (!validResult) {
    ReadPointerChain(pHandle, baseAddress, moduleOffset, offsets, 3, result, 4);
    res = *(float*)result;
    if (res < -350000 || res > 350000) {
      // invalid value
      std::this_thread::sleep_for(std::chrono::milliseconds(25));
    }
    else { // valid value
      validResult = true;
    }
  }
  free(result);
  return res;
}

float ReadPositionX(
  _In_ HANDLE pHandle,
  _In_ DWORD_PTR baseAddress,
  _In_ int moduleOffset
) {
  return ReadFloat(pHandle, baseAddress, moduleOffset, 0x10);
};

float ReadPositionY(
  _In_ HANDLE pHandle,
  _In_ DWORD_PTR baseAddress,
  _In_ int moduleOffset
) {
  return ReadFloat(pHandle, baseAddress, moduleOffset, 0x14);
};

float ReadHeading(
  _In_ HANDLE pHandle,
  _In_ DWORD_PTR baseAddress,
  _In_ int moduleOffset
) {
  float originalValue = ReadFloat(pHandle, baseAddress, moduleOffset, 0x08);
  float calculatedAngle =
    (-2.21468f * powf(10, -12) * powf(originalValue, 8)) +
                      (377.92f * powf(originalValue, 7)) +
     (4.14957f * powf(10, -12) * powf(originalValue, 6)) +
                     (-534.14f * powf(originalValue, 5)) +
       (2.359f * powf(10, -12) * powf(originalValue, 4)) + 
                      (242.87f * powf(originalValue, 3)) +
     (4.40536f * powf(10, -13) * powf(originalValue, 2)) +
                              (93.3503f * originalValue) +
                                                     180;
  return calculatedAngle;
}

void GenerateRandomString(char* s, const int len) {
  static const char alphanum[] =
    "0123456789"
    "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
    "abcdefghijklmnopqrstuvwxyz";

  srand((unsigned int)time(NULL));
  for (int i = 0; i < len; ++i) {
    s[i] = alphanum[rand() % (sizeof(alphanum) - 1)];
  }

  s[len] = 0;
}

time_t Filetime2TimeT(const FILETIME& ft) { ULARGE_INTEGER ull;   ull.LowPart = ft.dwLowDateTime;   ull.HighPart = ft.dwHighDateTime;   return ull.QuadPart / 10000000ULL - 11644473600ULL; }

std::vector<BYTE> ReadFile(wstring filename)
{
  // open the file:
  std::streampos fileSize;
  std::ifstream file(filename, std::ios::binary);

  // get its size:
  file.seekg(0, std::ios::end);
  fileSize = file.tellg();
  file.seekg(0, std::ios::beg);

  // read the data:
  std::vector<BYTE> fileData(fileSize);
  file.read((char*)&fileData[0], fileSize);
  return fileData;
}

#define CURL_STATICLIB
#include <curl\curl.h>

int main() {
  setlocale(LC_ALL, "en_US.utf8");

  DWORD pid = FindProcessIDByName(TEXT("FactoryGame-Win64-Shipping.exe"));
  
  HANDLE hModuleSnap = INVALID_HANDLE_VALUE;
  MODULEENTRY32 me32;
  hModuleSnap = CreateToolhelp32Snapshot(TH32CS_SNAPMODULE, pid);
  me32.dwSize = sizeof(MODULEENTRY32);
  Module32First(hModuleSnap, &me32);
  DWORD_PTR baseAddress = (DWORD_PTR)me32.modBaseAddr;
  DWORD_PTR pSize = me32.modBaseSize;
  CloseHandle(hModuleSnap);
  
  HANDLE pHandle = OpenProcess(PROCESS_ALL_ACCESS, FALSE, pid);
  int moduleOffset = 0x03AE8B70;
  
  float lastPositionX = 0, lastPositionY = 0;
  float maxDelta = 100;
  unsigned int consecutiveGoodRequired = 5;
  unsigned int consecutiveGood = 0;
  CURL* curlPos;
  CURL* curlSavebytes;
  CURLcode res;
  char username[6];
  GenerateRandomString(username, 5);
  username[5] = '\0';
  printf("Username: %s\n", username);
  curlPos = curl_easy_init();
  curlSavebytes = curl_easy_init();
  if (curlPos) {
    curl_easy_setopt(curlPos, CURLOPT_CUSTOMREQUEST, "POST");
    curl_easy_setopt(curlPos, CURLOPT_URL, "torsten.cz:29007/submit/427");
    curl_easy_setopt(curlPos, CURLOPT_FOLLOWLOCATION, 1L);
    curl_easy_setopt(curlPos, CURLOPT_DEFAULT_PROTOCOL, "http");
    struct curl_slist* headers = NULL;
    headers = curl_slist_append(headers, "Content-Type: application/json");
    curl_easy_setopt(curlPos, CURLOPT_HTTPHEADER, headers);
    char data[250];

    chrono::system_clock::time_point lastTime = chrono::system_clock::from_time_t(0);
    while (true) {
      // if elapsed more than 5 minutes from last savefile submission, submit
      chrono::duration<double> elapsedSeconds = chrono::system_clock::now() - lastTime;
      if (elapsedSeconds.count() > 60) {
        lastTime = chrono::system_clock::now();
        // get newest savefile
        wchar_t* localAppData = 0;
        SHGetKnownFolderPath(FOLDERID_LocalAppData, 0, NULL, &localAppData);

        std::wstringstream ss;
        ss << localAppData << L"\\FactoryGame\\Saved\\SaveGames\\";
        wstring saveGamesPath = ss.str();
        CoTaskMemFree(static_cast<void*>(localAppData));

        WIN32_FIND_DATA data;
        HANDLE hFind = FindFirstFile((saveGamesPath + L"*").c_str(), &data);
        wstring foundDir = L"";
        if (hFind != INVALID_HANDLE_VALUE) {
          do {
            wstring filename = data.cFileName;
            if (!filename.empty() && filename.find_first_not_of(L"0123456789") == wstring::npos) {
              foundDir = saveGamesPath + filename + L"\\";
              break;
            }
          } while (FindNextFile(hFind, &data));
          FindClose(hFind);
        }
        if (!foundDir.empty()) {
          // found a valid savefile directory
          hFind = FindFirstFile((foundDir + L"*").c_str(), &data);
          wstring newestFile = L"";
          time_t newestTime = 0;
          if (hFind != INVALID_HANDLE_VALUE) {
            do {
              time_t writeTime = Filetime2TimeT(data.ftLastWriteTime);
              wstring filename = data.cFileName;
              if (filename != L"." && filename != L".." && writeTime >= newestTime && filename.find(L".sav") != wstring::npos) {
                newestTime = writeTime;
                newestFile = foundDir + data.cFileName;
              }
            } while (FindNextFile(hFind, &data));
            FindClose(hFind);
          }
          if (!newestFile.empty()) {
            // we got a new savefile, good
            vector<BYTE> savebytes = ReadFile(newestFile);
            if (curlSavebytes) {
              curl_easy_setopt(curlSavebytes, CURLOPT_CUSTOMREQUEST, "POST");
              curl_easy_setopt(curlSavebytes, CURLOPT_URL, "torsten.cz:29007/submit/427/savefile");
              curl_easy_setopt(curlSavebytes, CURLOPT_FOLLOWLOCATION, 1L);
              curl_easy_setopt(curlSavebytes, CURLOPT_DEFAULT_PROTOCOL, "http");
              struct curl_slist* headers = NULL;
              headers = curl_slist_append(headers, "Content-Type: application/octet-stream");
              curl_easy_setopt(curlSavebytes, CURLOPT_HTTPHEADER, headers);
              curl_easy_setopt(curlSavebytes, CURLOPT_POSTFIELDSIZE_LARGE, savebytes.size());
              curl_easy_setopt(curlSavebytes, CURLOPT_POSTFIELDS, savebytes.data());
              res = curl_easy_perform(curlSavebytes);
              if (res == CURLE_OK) {
                printf("Savefile successfuly uploaded\n");
              }
              else {
                printf("Unable to upload savefile\n");
              }
            }
          }
        }
      }

      float positionX = ReadPositionX(pHandle, baseAddress, moduleOffset);
      float positionY = ReadPositionY(pHandle, baseAddress, moduleOffset);
      float heading   = ReadHeading  (pHandle, baseAddress, moduleOffset);

      if (
        abs(lastPositionX - positionX) < maxDelta &&
        abs(lastPositionY - positionY) < maxDelta
        ) {
        // valid position
        consecutiveGood++;
        if (consecutiveGood >= consecutiveGoodRequired) {
          sprintf_s(data, "[\n{\n\"name\":\"%s\",\n\"position\":{\n\"x\":%f,\n\"y\":%f\n},\n\"heading\":%f\n}\n]", username, positionX, positionY, heading);
          curl_easy_setopt(curlPos, CURLOPT_POSTFIELDS, data);
          res = curl_easy_perform(curlPos);
          if (res == CURLE_OK) {
            printf("Location (X: %.1f, Y: %.1f, H: %.0f) successfuly submitted\n", positionX, positionY, heading);
            std::this_thread::sleep_for(std::chrono::milliseconds(250));
          }
          else {
            printf("Invalid response\n");
          }
        }
      }
      else {
        consecutiveGood = 0;
      }
      lastPositionX = positionX;
      lastPositionY = positionY;
      std::this_thread::sleep_for(std::chrono::milliseconds(25));
    }
  }
  else {
    printf("Unable to initialize curl\n");
  }
  
  curl_easy_cleanup(curlPos);

}