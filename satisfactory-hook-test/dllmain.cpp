//#define WIN32_LEAN_AND_MEAN

#include <Windows.h>
#include "mhook-lib\mhook.h"
#include <iostream>
#include <vector>
#include <TlHelp32.h>
//#include <tchar.h>
//#include <chrono>
#include <thread>
#include <wchar.h>
using namespace std;

DWORD FindProcessIDByName(LPCTSTR ProcessName)
{
  PROCESSENTRY32 pt;
  HANDLE hsnap = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
  pt.dwSize = sizeof(PROCESSENTRY32);
  if (Process32First(hsnap, &pt)) { // must call this first
    do {
      if (!lstrcmpi(pt.szExeFile, ProcessName)) {
        CloseHandle(hsnap);
        return pt.th32ProcessID;
      }
    } while (Process32Next(hsnap, &pt));
  }
  CloseHandle(hsnap); // close handle on failure
  return 0;
};
DWORD_PTR GetAddressFromSignature(HANDLE pHandle, DWORD_PTR pBaseAddress, DWORD_PTR pSize, vector<int> signature) {
  if (pBaseAddress == NULL || pHandle == NULL) {
    return NULL;
  }
  std::vector<byte> memBuffer(pSize);
  if (!ReadProcessMemory(pHandle, (LPCVOID)(pBaseAddress), memBuffer.data(), pSize, NULL)) {
    std::cout << GetLastError() << std::endl;
    return NULL;
  }
  for (DWORD_PTR i = 0; i < pSize; i++) {
    for (DWORD_PTR j = 0; j < signature.size(); j++) {
      if (signature.at(j) != -1 && signature.at(j) != memBuffer.at(i + j))
        //std::cout << std::hex << signature.at(j) << std::hex << memBuffer[i + j] << std::endl;
        break;
      /*if (signature.at(j) == memBuffer.at(i + j) && j > 0)
        std::cout << std::hex << int(signature.at(j)) << std::hex << int(memBuffer.at(i + j)) << j << std::endl;*/
      if (j + 1 == signature.size())
        return pBaseAddress + i;
    }
  }
  return NULL;
};


typedef HRESULT(__stdcall* tHookedFunction) ();
// An instance of a tEndScene, representing the o(riginal)EndScene which is used as return from our hook.
tHookedFunction originalFunction;


HRESULT injectedFunction()
{
	MessageBox(NULL, L"Function called", L"Satisfactory Hook Test", NULL);
	// This makes the program flow go on, in other words if we don't do this, it'll crash
	return originalFunction();
}

void Main()
{
	MessageBox(NULL, L"Injected", L"Satisfactory Hook Test", NULL);
  WCHAR buffer[250];

  DWORD pid = FindProcessIDByName(TEXT("FactoryGame-Win64-Shipping.exe"));

  HANDLE hModuleSnap = INVALID_HANDLE_VALUE;
  MODULEENTRY32 me32;
  hModuleSnap = CreateToolhelp32Snapshot(TH32CS_SNAPMODULE, pid);
  me32.dwSize = sizeof(MODULEENTRY32);
  Module32First(hModuleSnap, &me32);
  DWORD_PTR baseAddress = (DWORD_PTR)me32.modBaseAddr;
  DWORD_PTR checkBaseAddress = (DWORD_PTR)GetModuleHandle(0);
  swprintf(buffer, 250, L"Polled base address:  %013llX\nActual base address:  %013llX", baseAddress, checkBaseAddress);
  MessageBox(NULL, (LPCWSTR)buffer, L"Satisfactory Hook Test", NULL);
  DWORD_PTR pSize = me32.modBaseSize;
  CloseHandle(hModuleSnap);

  HANDLE pHandle = OpenProcess(PROCESS_ALL_ACCESS, FALSE, pid);
  int moduleOffset = 0x040ED8E8;

  vector<int> signature{ 0x0F, 0x11, -1, -1, -1, -1, -1, 0x45, -1, -1, 0x44, -1, -1, -1, -1, -1, 0x00, 0x00 };
  DWORD_PTR signaturePointer = GetAddressFromSignature(pHandle, baseAddress, pSize, signature);
  swprintf(buffer, 250, L"Signature address:  %013llX\nOffset from module base: %013llX", signaturePointer, signaturePointer - baseAddress);
  MessageBox(NULL, (LPCWSTR)buffer, L"Satisfactory Hook Test", NULL);

  DWORD_PTR originalFunctionAddress = signaturePointer;
	originalFunction = (tHookedFunction)(originalFunctionAddress);

	Mhook_SetHook((PVOID*)&originalFunction, injectedFunction);
}

BOOL WINAPI DllMain(HINSTANCE hInst, DWORD dwReason, LPVOID lpReserved)
{
	if (dwReason == DLL_PROCESS_ATTACH)
	{
		CreateThread(0, 0, (LPTHREAD_START_ROUTINE)Main, 0, 0, 0);
  }
  else if (dwReason == DLL_PROCESS_DETACH) {
    Mhook_Unhook((PVOID*)&originalFunction);
  }

	return TRUE;
}